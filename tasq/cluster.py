"""
クラスタとコマンドをやり取りするモジュール
"""
import os
import re
import subprocess
import time
from tasq import utils


def execute(command, verbose=False, env=None):
    """
    引数の文字列をコマンドとして実行する。プロセスが作成できなかった場合、5秒ごとに最大10回まで再度実行する。
    :param str command: コマンド文字列
    :param bool verbose: 実行したコマンドを、標準出力に出力するかどうか
    :param env: 実行の際に設定する環境変数
    """
    if env is None:
        env = {}

    full_tokens = ['/bin/bash', '-c', command]
    my_env = os.environ.copy()
    my_env.update(env)

    # コマンドを出力
    if verbose:
        print(' '.join(full_tokens))

    max_try_num = 10
    out = None

    # プロセスが作成できなければエラー
    i = max_try_num
    while i > 0:
        utils.logger.debug('[execute]', ' '.join(full_tokens))
        p = subprocess.Popen(full_tokens, encoding='utf-8', env=my_env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        # エラーが返ってこなかったらok
        if len(err) == 0:
            break
        else:
            i -= 1
            print(type(err))
            print(err)
            time.sleep(5)

    if i == 0:
        print('Execution has failed {0} times.'.format(max_try_num))
        utils.logger.error('Execution has failed {0} times.'.format(max_try_num))

    return out


def throw_job(command=None, job=None, verbose=False, mode='original', **kwargs):
    """
    引数で指定されたコマンドとオプションでジョブを投入する。
    ジョブ投入には、パッケージに含まれている自作のスクリプトを使用している。
    :param Job job: 投入するジョブ
    :param str command: ジョブとして投入するコマンド
    :param bool verbose: Trueを指定すると、ジョブ投入時にコマンドを出力する
    :param str mode: 'qcmd'を指定すると、ジョブ投入に吉田先生が作成したqcmdを用いる
    """
    # default value
    memory = 5
    wall_time = 24
    process_num = 1
    nodes = 1
    job_name = 'tasq'

    if job is None and command is not None:
        # Jobインスタンスが指定されていない
        # Memory : int, 1-100[GB]
        if 'M' in kwargs:
            memory = kwargs['M']
        elif 'memory' in kwargs:
            memory = kwargs['memory']
        assert type(memory) == int and (1 <= memory <= 100)

        # WallTime : int, 1-336[h]
        if 'W' in kwargs:
            wall_time = kwargs['W']
        elif 'wall_time' in kwargs:
            wall_time = kwargs['wall_time']
        assert type(wall_time) == int and (1 <= wall_time <= 336)

        # Number of Process
        if 'P' in kwargs:
            process_num = kwargs['P']
        elif 'process_num' in kwargs:
            process_num = kwargs['process_num']
        assert type(process_num) == int and (1 <= process_num <= 20)

        assert process_num * memory <= 120

        # Job Name : str, [a-zA-Z0-9]+
        if 'N' in kwargs:
            job_name = kwargs['N']
        elif 'job_name' in kwargs:
            job_name = kwargs['job_name']
        assert type(job_name) == str and re.fullmatch('[A-Za-z0-9-_/#()]+', job_name)

    elif job is not None:
        memory = job.memory
        wall_time = job.wall_time
        job_name = job.job_name
        command = job.command
    else:
        raise Exception

    # make command
    if mode == 'qcmd':
        script_file = 'bash /gpfs/work/my016/tool/qcmd.sh'
        job_command = 'M={0} W={1} J={2} {3} {4}'.format(memory, wall_time, job_name, script_file, command)
        return _throw_job(job_command, verbose, {'BASH_ENV': '/home/0/y153331/aliases.txt'})
    elif mode == 'original':
        script_file = utils.get_full_path('tasq.sh')
        job_command = 'qsub -l nodes={nodes}:ppn={ppn},' \
                      'pmem={pmem}gb,pvmem={pmem}gb,mem={mem}gb,walltime={wall_time}:00:00 -N {job_name} -V {script}'\
            .format(ppn=process_num, nodes=nodes, pmem=memory, mem=process_num * memory,
                    wall_time=wall_time, job_name=job_name, script=script_file)
        # print(job_command)
        # print(command)
        return _throw_job(job_command, verbose, env={'JOB_CMD': command})
    else:
        raise Exception


def _throw_job(command, verbose=False, env=None):
    """
    ジョブ投入に失敗することがあるので、繰り返し投入する
    :param command: ジョブとして投入するコマンドの文字列
    :param verbose: 投入したジョブを標準出力に出力するかどうか
    :param env: 設定する環境変数
    :return: 投入したジョブのID(int)
    """
    if env is None:
        env = {}

    while True:
        out = execute(command, verbose=verbose, env=env)
        m = re.search(r'\d{6,7}(?=\.)', out)
        if m is not None:
            out = int(m.group(0))
            return out

        time.sleep(10)


def get_executing_jobs():
    """
    実行中のジョブを得る
    :return: 実行中ジョブのID(int)のset
    """
    command = 'qstat'
    out = execute(command)
    m = re.findall('\d{6,7}(?=\.)', out)

    result = set(map(int, m))
    return result


def delete_job(job_id):
    """
    指定したIDのジョブを削除する
    :param int job_id: 削除するジョブのID
    """
    command = 'qdel {0}'.format(job_id)
    execute(command)


def delete_all_job():
    """
    自分が投稿したジョブを全て削除する
    """
    jobs = get_executing_jobs()
    if len(jobs) == 0:
        return

    job_str = ' '.join(map(str, jobs))
    command = 'qdel {0}'.format(job_str)
    execute(command)
