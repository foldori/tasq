import copy
from tasq import utils


class TaskGraph:
    """
    タスクの順序を表すグラフを扱うクラス
    """

    def __init__(self, command_list=None, memory=5, wall_time=24, process_num=1, job_name='tasq-job'):
        """
        :param Union[str, List[str]] command_list:
        :param Union[int, List[int]] memory:
        :param int wall_time:
        :param int process_num:
        :param str job_name:
        """
        # Job IDからJobインスタンスへのマップ
        self.id_job_map = {}

        # Node ID とNodeインスタンスの辞書
        self.nodes = {0: Node(0, [], self)}

        # 次にノードを追加するときに使用するNode ID
        self.next_id = 1

        if command_list is not None:
            if type(command_list) == str:
                command_list = [command_list]

            self.add_node(command_list, memory=memory, wall_time=wall_time, process_num=process_num, job_name=job_name)

    def __str__(self):
        """
        グラフ全体を見やすい文字列にする
        """
        ret = ''
        for node_id, node in self.nodes.items():
            ret += str(node)
            ret += '\n'
        return ret[:-1]

    def add_node(self, command_list, parents=None, memory=5, wall_time=24, process_num=1, job_name='tasq-job'):
        """
        ノードを追加する
        :param str job_name:
        :param int process_num:
        :param int wall_time:
        :param Union[int, List[int]] memory:
        :param Union[str, List[str]] command_list:
        :param Union[int, set[int]] parents:
        """
        if parents is None:
            parents = {0}

        node = Node(self.next_id, command_list, self, parents,
                    memory=memory, wall_time=wall_time, process_num=process_num, job_name=job_name)
        self.nodes[self.next_id] = node
        self.next_id += 1

    def move_id(self, offset):
        """
        このグラフに属するノードのID全てをoffsetの値だけずらす
        :param int offset:
        """
        for node_id, node in self.nodes.items():
            node.move_id(offset)

    def get_node_num(self):
        return len(self.nodes) - 1

    def merge_parallel(self, graph):
        """
        このグラフ全体に対して、このグラフと完全に並列に実行してよいグラフをマージする
        :param TaskGraph graph: 追加するグラフ
        """
        offset = self.next_id - 1
        self.next_id += graph.get_node_num()
        for node_id, node in graph.nodes.items():
            # 根ノードはスキップ
            if node_id == 0:
                continue

            new_node_id = node_id + offset
            node.move_id(offset)
            node.graph = self
            self.nodes[new_node_id] = node

    def merge_subseq(self, graph):
        """
        このグラフに含まれるノードが全て完了した後、渡されたグラフが実行されるようにグラフをマージする
        このグラフの、どのノードの親にもなっていないノードの集合を、渡されたグラフで最初に実行されるノードそれぞれの親ノードに設定する
        :param TaskGraph graph: 追加するグラフ
        """
        new_parent_nodes = self._get_final_nodes()
        first_nodes = graph._get_first_nodes()

        offset = self.next_id - 1
        self.next_id += graph.get_node_num()
        for node_id, node in graph.nodes.items():
            if node_id == 0:
                continue

            new_node_id = node_id + offset
            node.move_id(offset)
            node.graph = self
            # 最初に実行されるノードであれば、親を書き換える
            if node_id in first_nodes:
                node.parents = copy.deepcopy(new_parent_nodes)
            self.nodes[new_node_id] = node

    def _get_first_nodes(self):
        """
        根ノードが親ノードとなっているノードIDの集合を返す
        :rtype set[int]:
        """
        result = set()
        for node_id, node in self.nodes.items():
            # 根ノードでなく、親ノードの集合に根ノード1つのみが含まれるノード
            if node_id != 0 and len(node.parents) == 1 and 0 in node.parents:
                result.add(node_id)

        return result

    def _get_final_nodes(self):
        """
        他のどのノードの親にもなっていない（必ず最後に実行される）ノードのIDの集合を返す
        :rtype set[int]:
        """
        # 最初は全てのノードが候補
        result = set(self.nodes.keys())

        for node_id, node in self.nodes.items():
            if node_id != 0:
                # 各ノードの親になっているノードは候補から除く
                result.difference_update(node.parents)

        return result

    def get_throwable_jobs(self):
        """
        投入可能なジョブのリストを返す
        :return: Jobのリスト
        :rtype list[Job]:
        """
        job_list = []
        for node_id, node in self.nodes.items():
            job_list.extend(node.get_throwable_jobs())

        return job_list

    def notify_finished_jobs(self, finished_id_set):
        """
        終了したジョブについて、終了済みフラグを立てる
        :param Set[int] finished_id_set: 終了したジョブのJob IDのset
        """
        for job_id, job in filter(lambda x: x[0] in finished_id_set, self.id_job_map.items()):
            job.finish()

    def register_job(self, job):
        """
        Job IDが割り当てられたジョブを、JobIDから逆引きできるようにマップに登録する
        :param Job job: ジョブ
        """
        assert job.job_id is not None
        self.id_job_map[job.job_id] = job

    def is_finished(self):
        """
        このグラフに含まれるジョブが全て完了していればTrueを返す
        """
        for node_id, node in self.nodes.items():
            if not node.is_finished():
                return False
        return True

    def remove_job(self, func):
        """
        条件を指定し、これを見たすジョブを削除する
        """
        for _, node in self.nodes.items():
            node.remove_job(func)

    def get_job_num(self):
        """
        このグラフに含まれるジョブの数を返す
        """
        job_num = 0
        for node_id, node in self.nodes.items():
            job_num += node.get_job_num()
        return job_num


class Node:
    """
    グラフのノードを表すクラス
    """

    def __init__(self, node_id, commands, graph, parents=None,
                 memory=5, wall_time=24, process_num=1, job_name='tasq-job'):
        """
        :param int node_id:
        :param Union[str, list[str]] commands:
        :param TaskGraph graph:
        :param Union[int, set[int]] parents:
        :param Union[int, List[int]] memory:
        """
        if parents is None:
            parents = {0}

        self.node_id = node_id
        self.remain_job_count = len(commands)
        self.graph = graph
        self.jobs = []

        if type(parents) == int:
            parents = {parents}
        self.parents = parents

        if type(commands) == str:
            commands = [commands]

        if type(memory) == int:
            memory = [memory] * len(commands)

        assert len(memory) == len(commands)

        for command, mem in zip(commands, memory):
            self.jobs.append(
                Job(command, self, memory=mem, wall_time=wall_time, process_num=process_num, job_name=job_name))

    def __str__(self):
        """
        :rtype str:
        """
        if self.node_id != 0:
            p_str = ' '.join(map(str, self.parents))
            j_str = '\n'.join(map(lambda x: '"' + str(x) + '"', self.jobs))
            f_str = 'finished' if self.is_finished() else ''
            return '{0:4} -> {{{1}}} {2} {3}'.format(self.node_id, p_str, j_str, f_str)
        else:
            return '{0:4} -> root'.format(self.node_id)

    def get_throwable_jobs(self):
        """
        このノードの親ノードが全て終了しているならば、ジョブのうちまだ投入されていないものを返す
        :rtype list[Job]:
        :return: 投入可能なジョブのリスト
        """
        for p_id in self.parents:
            if not self.graph.nodes[p_id].is_finished():
                return []
        return list(filter(lambda job: not job.is_threw(), self.jobs))

    def decrease_count(self):
        """
        ノードに属するジョブが1つ終了したときの処理
        """
        if self.remain_job_count > 0:
            self.remain_job_count -= 1
        else:
            print('Error: All Job has finished')

    def is_finished(self):
        """
        このノードに含まれるジョブが全て終了したかどうかを返す
        :return: 全て完了していればTrue
        """
        return self.remain_job_count == 0

    def move_id(self, offset):
        """
        ノードのIDをoffsetの値だけずらす
        :param int offset: ずらす値
        """
        self.node_id += offset

        # 親ノードのIDもずらす
        tmp_parents = set()
        for p_id in self.parents:
            if p_id == 0:
                tmp_parents.add(p_id)
            else:
                tmp_parents.add(p_id + offset)

        self.parents = tmp_parents

    def remove_job(self, func):
        """
        条件を満たすジョブを削除する
        """
        tmp_jobs = []
        for job in self.jobs:
            if func(*utils.parse_args(job.command)):
                tmp_jobs.append(job)
        self.jobs = tmp_jobs

    def get_job_num(self):
        """
        このノードのジョブ数を返す
        """
        return len(self.jobs)


class Job:
    """
    ジョブ1つを表すクラス
    """

    def __init__(self, command, node, memory=5, wall_time=24, process_num=1, job_name='tasq-job'):
        """
        :param str command: ジョブのコマンド
        :param Node node: このジョブが属するNodeのインスタンス
        :param int memory:
        :param int wall_time:
        :param int process_num:
        :param str job_name:
        """
        self.command = command
        self.memory = memory
        self.wall_time = wall_time
        self.process_num = process_num
        self.job_name = job_name
        self._node = node
        self.job_id = None
        self._threw = False

    def __str__(self):
        return 'memory:{0} time:{1} {2}'.format(self.memory, self.wall_time, self.command)
        # return self.command

    def dispatch_job_id(self, job_id):
        """
        ジョブに対して、ジョブ投入時に得たJob IDを割り当てる
        このときに、graphのid_job_mapに登録しておく
        :param int job_id: ジョブID
        """
        self.job_id = job_id
        self._threw = True
        self._node.graph.register_job(self)

    def is_threw(self):
        return self._threw

    def finish(self):
        """
        ジョブが1つ終了したことを、このジョブが属するノードに通知する
        """
        self._node.decrease_count()
