import argparse
import time
import glob
import os
import re

from .cluster import get_executing_jobs, throw_job, delete_job, delete_all_job
from .dbutils import has_waiting_jobs, get_data, update_data, clear_data
from . import utils


def default(args):
    """
    引数で渡されたコマンドをジョブとして即投入する。
    """
    if args.execute is None:
        return
    else:
        command = args.execute
    throw_job(command, verbose=True)


def stop(_):
    """
    データファイルに待機しているグラフを全て削除する
    """
    clear_data()


def remove(args):
    """
    グラフごとジョブを削除する
    """
    # rm -a/--all
    # 全てのグラフとジョブを削除
    if args.all:
        clear_data()
        delete_all_job()

    # rm -n/--name
    # 指定された名前のグラフを削除
    elif args.name is not None:
        print('Not implemented')

    # rm -j/--job-id
    # 指定されたIDのジョブを削除
    elif args.job_id is not None:
        delete_job(args.job_id)


def show(args):
    """
    データファイル内にあるグラフを全て出力する
    """
    # ジョブの個数だけを出力する
    if args.number:
        command, thrown_jog_id_set, graph_list = get_data()
        print('Graphs:', len(graph_list))
        print('Jobs:', sum([graph.get_job_num() for graph in graph_list]))
        print('Executing jobs:', len(thrown_jog_id_set))

    else:
        command, thrown_jog_id_set, graph_list = get_data()
        print('Graphs:')
        for graph in graph_list:
            print(graph)

        print('Executing jobs:')
        for job_id in sorted(thrown_jog_id_set):
            print(job_id)


def remove_file(args):
    """
    ジョブを投入して作成されたファイルを削除する
    """
    files = glob.glob('./tasq-job.o*')
    error_str = 'Error'

    for file in sorted(files):
        rm_flag = True
        if args.check:
            with open(file, mode='rt') as rf:
                lines = rf.readlines()
            rm_flag = error_str not in ''.join(lines)
        # print(file, rm_flag)
        if rm_flag:
            # pass
            os.remove(file)


def grep(args):
    """
    出力ファイルのうち条件を満たすものを選び、コマンドの行に指定した加工を加えて出力
    """
    file_name = '{}.o*'.format(args.name)
    files = glob.glob(file_name)
    # files = glob.glob('./tasq-job.o*')
    cmd_pattern = '^python -m'
    cmd_list = []
    filename_list = []
    for file in sorted(files):

        # まず、指定されたエラーを含むファイルを選ぶ
        if args.error is not None:
            with open(file, mode='rt') as rf:
                lines = rf.readlines()
                select_flag = args.error in ''.join(lines)
        else:
            select_flag = True

        # 条件を満たすファイルをopen
        if select_flag:
            with open(file, mode='rt') as rf:
                for line in rf:
                    if re.match(cmd_pattern, line) is not None:
                        cmd_list.append(line.strip())
                        filename_list.append(file)
                        break
    # print(cmd_list)
    output_list = []
    for line, filename in zip(cmd_list, filename_list):
        cmd, arguments = utils.parse_args(line)

        # argumentで指定された条件がある場合、条件をすべて満たすかどうか判断する
        select_flag = True
        satisfy_args = [] if args.argument is None else args.argument
        for c in satisfy_args:
            # args.argumentの各要素は、サイズが1の場合には、0番目の要素はその名前の引数がフラグとして指定されたことを表す
            if len(c) == 1:
                t_arg_name = c[0]
                if t_arg_name in arguments:
                    pass
                else:
                    select_flag = False
            else:
                # サイズ2の場合には0番目が引数の名前、1番目が引数の値を表す
                t_arg_name = c[0]
                t_arg_value = c[1]
                # args.argumentで指定された引数名が指定されており、
                # その値の中に指定された値と一致するものが含まれていればよい
                if t_arg_name in arguments and t_arg_value in set(arguments[t_arg_name]):
                    pass
                else:
                    select_flag = False

        if select_flag:
            # print(line)
            # print(arguments)
            if args.select_filename:
                # -Fが指定された場合、ファイル名だけ出力
                output_list.append(filename)
            elif args.cut is None or len(args.cut) == 0:
                # 指定されなかった場合、全て出力
                output_list.append(line)
            else:
                # args.cutで指定された変数の値のみを出力
                r_list = []
                for c_var in args.cut:
                    if c_var in arguments:
                        r_list.append(' '.join(arguments[c_var]))

                if len(r_list) > 0:
                    output_list.append(' '.join(r_list))

    if args.distinct:
        output_list = set(output_list)

    if args.sorting:
        output_list = sorted(output_list)

    if args.output_append is not None and len(args.output_append) > 0:
        output_list = map(lambda x:  args.output_append + ' ' + x, output_list)

    if args.join is None:
        for out_str in output_list:
            print(out_str)
    else:
        print(args.join.join(output_list))


def run(args):
    # 指定された引数をチェックする
    assert 0 <= args.throw_interval
    assert 0 <= args.check_interval
    if args.max_job is not None:
        assert 1 <= args.max_job <= 10000

    if args.loop:
        while has_waiting_jobs():
            _run(args)
            time.sleep(args.check_interval)
    else:
        _run(args)


def _run(args):
    # 投入待ちのジョブが1つもなければ終了する
    if not has_waiting_jobs():
        return

    # データベースから、投入済みジョブとTaskGraphのリストを得る
    # thrown_job_id_set: 前回までに投入され、まだ終了していないジョブ
    status, thrown_job_id_set, graph_list = get_data()
    # print('waiting graphs:', graph_list)
    utils.logger.debug('waiting graphs:', str(graph_list))

    # 現在実行中のジョブを得る
    # executing_job_id_set: 現在実行中のジョブ
    # thrown_job_id_setに含まれているが、これに含まれていないジョブ→終了したジョブ
    executing_job_id_set = get_executing_jobs()
    # print('now executing:', executing_job_id_set)
    utils.logger.debug('executing jobs:', str(executing_job_id_set))

    # 投入可能なジョブの数を計算
    max_queue_length = utils.get_config('MaxQueueLength')
    if args.max_job is not None:
        max_queue_length = args.max_job

    throwable_num = max(0, max_queue_length - len(executing_job_id_set))
    utils.logger.debug('throwable num:', str(throwable_num))

    # 投入可能なジョブのリストを得る
    throwable_jobs = []
    for graph in graph_list:
        if len(throwable_jobs) < max_queue_length:
            throwable_jobs.extend(graph.get_throwable_jobs())

    utils.logger.debug('throwable jobs:', ' '.join(map(str, throwable_jobs)))

    # 投入可能な数だけ投入する
    # tmp_job_id_set: 今回投入したジョブを仮に保存しておく
    tmp_job_id_set = set()
    for i in range(min(throwable_num, len(throwable_jobs))):
        job = throwable_jobs[i]
        print(job.command)
        job_id = throw_job(job=job)
        tmp_job_id_set.add(job_id)
        # executing_job_id_set.add(job_id)

        # job IDを割り当てる
        job.dispatch_job_id(job_id)
        utils.logger.info('[throw] \t' + str(job_id) + '\t' + str(job))

        # 指定した時間だけ待つ
        time.sleep(args.throw_interval)

    # 終了済みジョブを算出する
    # not_queued_job_set = executing_job_id_set - thrown_job_id_set
    # continuing_job_set: 実行が終了していないジョブ
    continuing_job_set = thrown_job_id_set.intersection(executing_job_id_set)
    # continuing_job_set = set()
    continuing_job_set.update(tmp_job_id_set)
    # finished_job_id_set: 終了したジョブ
    finished_job_id_set = thrown_job_id_set - continuing_job_set
    for job_id in finished_job_id_set:
        utils.logger.info('[finish]\t' + str(job_id))

    # 終了済みジョブを、各グラフに通知する
    cont_graphs = []
    for graph in graph_list:
        graph.notify_finished_jobs(finished_job_id_set)
        if not graph.is_finished():
            cont_graphs.append(graph)

    # print('cont graph:', cont_graphs)
    # print('cont jobs:', continuing_job_set)
    utils.logger.debug('being processed jobs:', continuing_job_set)

    # 更新した投入済みジョブとグラフのリストを、データベースに書き込む
    update_data((None, continuing_job_set, cont_graphs))


def main():
    parser = argparse.ArgumentParser()

    # default
    parser.add_argument('-e', '--execute', type=str, help='与えられたコマンドをジョブとして投入する')
    parser.set_defaults(func=default)

    # Add sub parser
    subparsers = parser.add_subparsers(dest='parser_name')

    # tasq run
    parser_run = subparsers.add_parser('run', help='キューに格納されたジョブをクラスタに投入する')
    parser_run.add_argument('-l', '--loop', action='store_true', help='実行中のジョブが無くなるまで無限ループする')
    parser_run.add_argument('-i', '--throw-interval', type=float, default=0,
                            help='ジョブ投入ごとのインターバルを設定する。デフォルトは0秒')
    parser_run.add_argument('-c', '--check-interval', type=float, default=5,
                            help='ジョブの完了をチェックし残りを投入するときのインターバルを設定する。デフォルトは5秒。'
                                 '-lが指定されていない場合は無意味です')
    parser_run.add_argument('-j', '--max-job', type=int,
                            help='一度に投入するジョブの最大数を設定する。1以上10000以下でない場合、エラーで終了します')
    parser_run.set_defaults(func=run)

    # tasq rm
    parser_rm = subparsers.add_parser('rm', help='投入済みのジョブを削除する')
    parser_rm.set_defaults(func=remove)
    group_rm = parser_rm.add_mutually_exclusive_group(required=True)
    group_rm.add_argument('-a', '--all', action='store_true', help='実行中または待機中のジョブを全て削除する')
    group_rm.add_argument('-n', '--name', type=str, help='指定した名前のグラフを削除（未実装）')
    group_rm.add_argument('-j', '--job-id', type=int, help='指定されたIDのジョブを削除')

    # tasq stop
    parser_stop = subparsers.add_parser('stop', help='待機中のジョブのみを削除する')
    parser_stop.set_defaults(func=stop)

    # tasq ls
    parser_ls = subparsers.add_parser('ls', help='現在実行中または待機中のジョブを表示する(tasq showと同じ動作)')
    parser_ls.add_argument('-n', '--number', action='store_true', help='指定した場合、実行中と待機中のジョブの数を出力する')
    parser_ls.set_defaults(func=show)

    # tasq rmf
    parser_rmf = subparsers.add_parser('rmf', help='tasqでジョブを投入して作成されたファイルを削除する')
    parser_rmf.add_argument('-c', '--check', action='store_true', help='ファイル内にErrorという文字列を含むファイルを残す')
    parser_rmf.set_defaults(func=remove_file)

    # tasq grep
    parser_grep = subparsers.add_parser('grep', help='出力ファイルのうち条件を満たすものを選び、'
                                                     'コマンドの行に指定した加工を加えて出力')
    parser_grep.add_argument('-n', '--name', default='tasq-job', type=str,
                             help='出力ファイルに共通する名前')
    parser_grep.add_argument('-a', '--argument', nargs='+', action='append',
                             help='指定した引数名と値を持つコマンドのみを出力する')
    parser_grep.add_argument('-e', '--error', type=str, default=None,
                             help='指定した名前のエラーを含むファイルのみを対象とする')
    parser_grep.add_argument('-c', '--cut', nargs='*', type=str,
                             help='指定した名前の引数の値のみを切り抜いて出力')
    parser_grep.add_argument('-o', '--output-append', type=str, default='',
                             help='出力の際に、指定した文字列を先頭に付加して出力する')
    parser_grep.add_argument('-j', '--join', type=str, default=None,
                             help='None以外を指定した場合、指定した文字列でjoinして出力')
    parser_grep.add_argument('-s', '--sorting', action='store_true', help='指定した場合、出力を辞書順にソートする')
    parser_grep.add_argument('-d', '--distinct', action='store_true', help='指定した場合、全く同じ出力は除去される')
    parser_grep.add_argument('-F', '--select-filename', action='store_true', help='指定した場合、ファイル名を出力する')
    parser_grep.set_defaults(func=grep)

    args = parser.parse_args()
    # print(args)
    args.func(args)


if __name__ == '__main__':
    main()
