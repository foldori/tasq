#!/bin/bash
#PBS -l nodes=1:ppn=1
#PBS -q wLrchq
#PBS -j oe

cd $PBS_O_WORKDIR

date
hostname
echo "$JOB_CMD"
eval "$JOB_CMD"
date