import os
import pickle
import fcntl

from .utils import logger, get_full_path


# データファイルのパス
def _db_name():
    return get_full_path('database')


def init():
    """
    データファイルが存在しない場合に初期値で作成
    """
    if not os.path.exists(_db_name()):
        with open(_db_name(), mode='wb') as f:
            pickle.dump((None, set(), []), f)


def clear_data():
    """
    データファイルを初期化する
    """
    with open(_db_name(), mode='wb') as f:
        pickle.dump((None, set(), []), f)


def push_graph(graph):
    """
    引数で渡されたグラフをキュー（データベース）にpushする
    :param graph: pushするグラフ
    """
    f = open(_db_name(), 'r+b')
    try:
        fcntl.flock(f.fileno(), fcntl.LOCK_EX)
        command, thrown_jog_id_set, graph_list = pickle.load(f)
        graph_list.append(graph)

        # 0バイト目にシークし切り詰めることで、ファイルを空にする
        f.seek(0)
        f.truncate()

        pickle.dump((command, thrown_jog_id_set, graph_list), f)
        fcntl.flock(f.fileno(), fcntl.LOCK_UN)
    except IOError:
        logger.error('Lock Error')
    finally:
        f.close()


def has_waiting_jobs():
    """
    投入待ちのジョブがデータベースにあればTrueを、なければFalseを返す
    :return: 投入待ちのジョブがあればTrue、なければFalse
    """
    if not os.path.exists(_db_name()):
        return False

    command, _, graph_list = get_data()
    if len(graph_list) == 0:
        return False

    return True


def get_data():
    """
    データファイルをロックし、データを読み込んで返す
    :return: 読み込んだデータ
    """
    data = None
    f = open(_db_name(), 'rb')
    try:
        fcntl.flock(f.fileno(), fcntl.LOCK_SH)
        data = pickle.load(f)
        fcntl.flock(f.fileno(), fcntl.LOCK_UN)
    except IOError:
        logger.error('Lock Error')
    finally:
        f.close()

    return data


def update_data(data_tuple):
    """
    データファイルをロックし、データを上書きして返す
    """
    f = open(_db_name(), 'r+b')
    try:
        fcntl.flock(f.fileno(), fcntl.LOCK_EX)
        pickle.dump(data_tuple, f)
        fcntl.flock(f.fileno(), fcntl.LOCK_UN)
    except IOError:
        logger.error('Lock Error')
    finally:
        f.close()
