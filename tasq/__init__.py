from .core import remove, run, show, stop, default
from .cluster import execute, get_executing_jobs, throw_job, delete_all_job, delete_job
from .taskgraph import TaskGraph
from .dbutils import push_graph, get_data, clear_data, has_waiting_jobs, update_data
from . import utils

import logging


def init():
    import os
    import configparser
    config_path = utils.get_full_path('setting.ini')
    if not os.path.exists(config_path):
        default_config = configparser.ConfigParser()
        default_config['DEFAULT'] = {
            'MaxQueueLength': 300,
            'JobThrowInterval': 100
        }
        with open(config_path, mode='wt', encoding='UTF-8') as config_file:
            default_config.write(config_file)

    # Load setting.ini
    config = configparser.ConfigParser()
    config.read(config_path)
    return config['DEFAULT']


dbutils.init()
utils.config = init()
utils.logger = utils.get_logger(logging.INFO)
