import re
import logging
import logging.handlers
import pkg_resources

config = None
# logger: logging.Logger = None
logger = None


def get_config(var_name):
    return config.getint(var_name)


def get_full_path(filename):
    """
    文字列で渡されたdataディレクトリ内のファイルを、絶対パスに変換して返す
    :param str filename:
    :return: ファイルの絶対パス
    """
    return pkg_resources.resource_filename('tasq', 'data/' + filename)


def get_logger(level):
    log = logging.getLogger('tasq')
    log.setLevel(level)

    handler = logging.handlers.RotatingFileHandler('log.txt', maxBytes=104857600)
    handler.setLevel(level)
    formatter = logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s')
    handler.setFormatter(formatter)
    log.addHandler(handler)

    return log


def parse_args(line):
    """
    コマンドの文字列を雑にパースして返す
    :param str line:
    """
    line = re.sub(' +', ' ', line)
    terms = line.split(' ')
    cmd = ' '.join(terms[:3])

    args = {}
    arg_name = None
    arg_values = []
    for term in terms[3:]:
        m = re.match('^--?([a-zA-Z-]+)', term)

        # termが引数名の指定だった場合
        if m is not None:
            # 2番目以降のarg_nameである場合、既にある引数をargに追加する
            if arg_name is not None:
                args[arg_name] = arg_values
                # arg_name = None
                arg_values = []

            # arg_nameを更新
            arg_name = m.group(1)

        else:
            # arg_valueを現在のものに追加
            arg_values.append(term)
    else:
        if arg_name is not None:
            args[arg_name] = arg_values

    return cmd, args
