import logging
import logging.handlers

log = logging.getLogger('LogTest')
log.setLevel(logging.DEBUG)

handler = logging.StreamHandler()
handler = logging.handlers.RotatingFileHandler('log.txt', maxBytes=104857600)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s')
handler.setFormatter(formatter)

log.addHandler(handler)

log.debug('Debug Log')
log.info('Info Log')
log.warning('Warning Log')
log.error('Error Log')
log.critical('Critical Log')

