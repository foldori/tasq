from tasq import taskgraph
from tasq import dbutils

dbutils.clear_data()
g0 = taskgraph.TaskGraph('qcmd sleep 10')
g1 = taskgraph.TaskGraph('qcmd sleep 10')
g2 = taskgraph.TaskGraph('qcmd ls -lh')
g0.merge_subseq(g1)
g0.merge_subseq(g2)
print(g0)
dbutils.push_graph(g0)
tmp = dbutils.get_data()
print(tmp)
