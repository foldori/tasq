import tasq

commands = ['ls -lh', 'sleep 10']
graph = tasq.TaskGraph(commands, memory=2)
tasq.push_graph(graph)
