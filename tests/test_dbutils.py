from tasq import dbutils, taskgraph

dbutils.clear_data()
graph = taskgraph.TaskGraph(['ls -lh'])
graph.add_node('sleep 10')
graph.merge_subseq(taskgraph.TaskGraph('sleep 11'))
graph.merge_subseq(taskgraph.TaskGraph('sleep 12'))

dbutils.push_graph(graph)
print(graph)
